var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  walker: [0, 0, 0],
  path: [128, 128, 128],
  back: [64, 64, 64],
  walker: [255, 255, 255]
}

var size = 29
var boardSize
var mazeMap = create2DArray(size, size, 1, true)
var walker = {
  x: null,
  y: null
}
var neighbours = []
walker.x = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
walker.y = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
mazeMap[walker.x][walker.y] = 0
var backTrack = []
var inProg = true
var frames = 0

var possibleStartPos
var numOfWalkers = 61
var walkers = []
var state = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  while (inProg === true) {
    neighbours = getNeighbours(walker.x, walker.y, mazeMap, 1, 2)
    var rand = Math.floor(Math.random() * neighbours.length)
    if (neighbours[rand] !== undefined) {
      backTrack.push([walker.x, walker.y])
      walker.x = neighbours[rand][0]
      walker.y = neighbours[rand][1]
      mazeMap[walker.x][walker.y] = 0
      if (neighbours[rand][2] === 0) {
        mazeMap[walker.x - 1][walker.y] = 0
        backTrack.push([walker.x - 1, walker.y])
      } else if (neighbours[rand][2] === 1) {
        mazeMap[walker.x + 1][walker.y] = 0
        backTrack.push([walker.x + 1, walker.y])
      } else if (neighbours[rand][2] === 2) {
        mazeMap[walker.x][walker.y - 1] = 0
        backTrack.push([walker.x, walker.y - 1])
      } else if (neighbours[rand][2] === 3) {
        mazeMap[walker.x][walker.y + 1] = 0
        backTrack.push([walker.x, walker.y + 1])
      }
    } else {
      if (backTrack.length !== 0) {
        walker.x = backTrack[backTrack.length - 2][0]
        walker.y = backTrack[backTrack.length - 2][1]
        backTrack.splice(-2, 2)
      } else {
        inProg = false
      }
    }
  }
  possibleStartPos = getPossibleStartPos(mazeMap)
  for (var i = 0; i < numOfWalkers; i++) {
    var randomPos = Math.floor(Math.random() * possibleStartPos.length)
    walkers.push(possibleStartPos[randomPos])
    possibleStartPos.splice(randomPos, 1)
  }
  for (var i = 0; i < walkers.length; i++) {
    mazeMap[walkers[i][0]][walkers[i][1]] = 3
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)
  fill(colors.path)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, (42 / 768) * boardSize * (17 / size) * (size - 1), (42 / 768) * boardSize * (17 / size) * (size - 1))

  for (var i = 0; i < mazeMap.length; i++) {
    for (var j = 0; j < mazeMap[i].length; j++) {
      if (mazeMap[i][j] === 1) {
        noStroke()
        if (state === 0) {
          fill(colors.path)
        } else {
          fill(colors.dark)
        }
        rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size))
      }
      if (mazeMap[i][j] === 0) {
        noStroke()
        fill(colors.dark)
        rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size))
      }
      if (mazeMap[i][j] > 2) {
        noStroke()
        fill(colors.walker[0], colors.walker[1], colors.walker[2], mazeMap[i][j] - (2 + mouseY / windowHeight))
        rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (42 / 768) * boardSize * (17 / size), (42 / 768) * boardSize * (17 / size))
      }
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0
    for (var i = 0; i < walkers.length; i++) {
      neighbours = getNeighbours(walkers[i][0], walkers[i][1], mazeMap, 0, 1)
      var rand = Math.floor(Math.random() * neighbours.length)
      if (neighbours[rand] !== undefined) {
        walkers[i][0] = neighbours[rand][0]
        walkers[i][1] = neighbours[rand][1]
        mazeMap[walkers[i][0]][walkers[i][1]] = 3
      }
    }
    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        if (mazeMap[i][j] > 2) {
          mazeMap[i][j] -= 0.02 + 0.48 * (1 - mouseX / windowWidth)
        } else if (mazeMap[i][j] <= 2 && mazeMap[i][j] > 1) {
          mazeMap[i][j] = 0
        }
      }
    }
  }
}

function mousePressed() {
  if (state === 0) {
    state = 1
  } else {
    state = 0
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getPossibleStartPos(array) {
  var pos = []
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === 0) {
        pos.push([i, j])
      }
    }
  }
  return pos
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(x, y, array, init, step) {
  var neighbour = []
  if (x < array.length - step) {
    if (array[x + step][y] === init) {
      neighbour.push([x + step, y, 0])
    }
  }
  if (x > step) {
    if (array[x - step][y] === init) {
      neighbour.push([x - step, y, 1])
    }
  }
  if (y < array.length - step) {
    if (array[x][y + step] === init) {
      neighbour.push([x, y + step, 2])
    }
  }
  if (y > step) {
    if (array[x][y - step] === init) {
      neighbour.push([x, y - step, 3])
    }
  }
  return neighbour
}
